/**
 * 
 */
package com.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.dao.DogDAO;
import com.model.Dog;


/**
 * @author alan.silva
 *
 */

@Stateless
public class DogFacadeImp implements DogFacade {

	@EJB
	private DogDAO dogDAO;	
	
	/* (non-Javadoc)
	 * @see com.facade.DogFacade#save(com.model.Dog)
	 */
	@Override
	public void save(Dog dog) {
		isDogWithAllData(dog);
		
		dogDAO.save(dog);

	}

	private void isDogWithAllData(Dog dog) {
		boolean hasError = false;
		
		if(dog == null){
            hasError = true;
        }
 
        if (dog.getName() == null || "".equals(dog.getName().trim())){
            hasError = true;
        }
 
        if(dog.getWeight() <= 0){
            hasError = true;
        }
 
        if (hasError){
            throw new IllegalArgumentException("The dog is missing data. Check the name and weight, they should have value.");
        }
		
	}

	/* (non-Javadoc)
	 * @see com.facade.DogFacade#update(com.model.Dog)
	 */
	@Override
	public Dog update(Dog dog) {
		isDogWithAllData(dog);		
		return dogDAO.update(dog);
	}

	/* (non-Javadoc)
	 * @see com.facade.DogFacade#delete(com.model.Dog)
	 */
	@Override
	public void delete(Dog dog) {
	   dogDAO.delete(dog);
	}

	/* (non-Javadoc)
	 * @see com.facade.DogFacade#find(int)
	 */
	@Override
	public Dog find(int entityID) {
		// TODO Auto-generated method stub
		return dogDAO.find(entityID);
	}

	/* (non-Javadoc)
	 * @see com.facade.DogFacade#findAll()
	 */
	@Override
	public List<Dog> findAll() {
		// TODO Auto-generated method stub
		return dogDAO.findAll();
	}

}
