/**
 * 
 */
package com.dao;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;



/**
 * @author alan.silva
 *
 */


public abstract class GenericDAO<T> {
	
	private final static String UNIT_NAME = "CrudPU";
	
	@PersistenceContext(unitName = UNIT_NAME)
	private EntityManager em;
	
	private Class<T> entityClass;
	
	public GenericDAO(Class<T> entityClass){
		this.entityClass = entityClass;
	}
	
	public void save(T entity){
		em.persist(entity);
	}
	
	protected void delete(Object id, Class<T> classe ){
		T entityToBeRemoved = em.getReference(classe, id);
		
		em.remove(entityToBeRemoved);
	}
	
	public T update(T entity){
	  return em.merge(entity);
	}
	
	public T find(int entityID){
		return em.find(entityClass, entityID);
	}
	
	
	@SuppressWarnings({"unchecked","rawTypes"})
	public List<T> findAll(){
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return em.createQuery(cq).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	protected T findOneREsult(String nameQuery, Map<String, Object> parametres){
		T result = null;
		
		try{
			
			Query query = em.createNamedQuery(nameQuery);
			
			if (parametres != null && !parametres.isEmpty()){
				populateQueryParametres(query,parametres);
			}
			result = (T) query.getSingleResult();
		}catch(Exception e){
			System.out.println("Error while running query "+e.getMessage());
		}
		
		return result;
	}

	private void populateQueryParametres(Query query,
			Map<String, Object> parametres) {

		for (Entry<String, Object> entry : parametres.entrySet()){
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
	}
	
	
	
	
	

}
