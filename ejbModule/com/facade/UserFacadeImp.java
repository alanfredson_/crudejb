/**
 * 
 */
package com.facade;

import javax.ejb.EJB;

import javax.ejb.Stateless;

import com.dao.UserDAO;
import com.model.User;

/**
 * @author alan.silva
 *
 */
@Stateless
public class UserFacadeImp implements UserFacade {

	@EJB
	private UserDAO userDAO;

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userDAO.findUserByEmail(email);
	}
	
	
	
	
}
