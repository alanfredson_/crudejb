/**
 * 
 */
package com.dao;

import com.model.Dog;

/**
 * @author alan.silva
 *
 */
public class DogDAO extends GenericDAO<Dog> {

	public DogDAO(){
		super(Dog.class);
	}
	
	
	public void delete(Dog dog){
		super.delete(dog.getId(), Dog.class);
	}
	
	
}
