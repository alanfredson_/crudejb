/**
 * 
 */
package com.dao;

import java.util.HashMap;
import java.util.Map;

import com.model.User;

/**
 * @author alan.silva
 *
 */
public class UserDAO extends GenericDAO<User> {

	public UserDAO(){
		super(User.class);
	}
	
	public User findUserByEmail(String email){
		Map<String, Object> parametres = new HashMap<String, Object>();
		parametres.put("email", email);			
		
		return super.findOneREsult(User.FIND_BY_EMAIL, parametres); 
	}
	
	
}
